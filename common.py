import json


# read data
def read_data(path):
    file = open(path, "r")
    data = []

    for line in file:
        # print(line)
        sps = line.split(",")
        line = []
        if len(sps) >= 1:
            for i in range(len(sps)):
                try:
                    val = float(sps[i])
                except Exception as e:
                    print(e)
                    val = 0
                line.append(val)
            data.append(line)

    return data


def read_settings_info(path):
    with open(path, 'r') as jp:
        data = json.load(jp)
        return data
